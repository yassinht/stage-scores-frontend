import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) {}

  login(credentials: any): Observable<any> {
    return this.http.post<any>('your-login-endpoint', credentials);
  }

  getUserFromPlaceholder(): Observable<any> { // Add return type Observable<any>
    return this.http.get<any>("http://localhost:5000/");
  }

  private url = 'http://localhost:5000/';

  createNewprof(patient: any): Observable<any> { // Add return type Observable<any>
    return this.http.post<any>(this.url + 'patient', patient);
  }

  loginPat(data: any): Observable<any> { // Add return type Observable<any>
    return this.http.post<any>('http://localhost:5000/patient', data);
  }

  loginPro(data: any): Observable<any> { // Add return type Observable<any>
    return this.http.post<any>('http://localhost:5000/pro', data);
  }
}
