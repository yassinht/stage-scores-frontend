import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LayoutsRoutingModule } from './layouts-routing.module';
import { FrontLayoutComponent } from './front-layout/front-layout.component';
import { ProfLayoutComponent } from './prof-layout/prof-layout.component';
import { PatientLayoutComponent } from './patient-layout/patient-layout.component';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    FrontLayoutComponent,
    ProfLayoutComponent,
    PatientLayoutComponent
  ],
  imports: [
    CommonModule,
    LayoutsRoutingModule,
    RouterModule
  ]
})
export class LayoutsModule { }
