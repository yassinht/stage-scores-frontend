import { TestBed } from '@angular/core/testing';

import { AuthprofService } from './authprof.service';

describe('AuthprofService', () => {
  let service: AuthprofService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(AuthprofService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
