import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthprofService {

  constructor(private http: HttpClient) { }
  private url = 'http://localhost:5000/';

 createNewPatient(patient: any){
    // Utilisation correcte de la concaténation pour former l'URL
    return this.http.post(this.url + 'patient/register' , patient);
  }
}
