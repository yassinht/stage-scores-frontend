import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
@Injectable({
  providedIn: 'root'
})
export class PatientService {

    helper = new JwtHelperService();
    apiUrl = 'http://your-api-base-url/'
    constructor(private http: HttpClient) { }






    // *****************start Patient Service**************************************//

  loginSPat(body: any) {
    return this.http.post(`${this.apiUrl}patient/login`, body)


  }

  saveDataPat(token: any) {
    let decodeToken = this.helper.decodeToken(token)
    localStorage.setItem('token_Pat', token)
  }


  getNamePat() {
    let token: any = localStorage.getItem('token_Pat')
    let decodeToken = this.helper.decodeToken(token)
    return decodeToken.subject.name
  }
  LoggedInPat(){
    let token:any=localStorage.getItem('token_Pat')
    if(!token){
     return false
    }

    if(this.helper.isTokenExpired(token)){
      return false
    }

    return true
 }

  // *****************end Patient Service**************************************//



}
