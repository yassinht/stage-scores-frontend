import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class ProfService {
    helper = new JwtHelperService();
    apiUrl = 'http://your-api-base-url/'
    constructor(private http: HttpClient) { }

    // ***************** start Professionnel Service**************************************//

    loginSPro(body: any) {
        return this.http.post(`${this.apiUrl}doctor/login`, body);
    }

    saveDataPro(token: any) {
        let decodeToken = this.helper.decodeToken(token);
        localStorage.setItem('token_Pro', token);
        localStorage.setItem('role', decodeToken.subject.role);
        localStorage.setItem('name_Pro', decodeToken.subject.name);
    }

    getNamePro() {
        let token: any = localStorage.getItem('token_Pro');
        let decodeToken = this.helper.decodeToken(token);
        return decodeToken.subject.name;
    }

    functionOne(i, tab) {
        var index = tab.findIndex(s => s.type === i);
        /* console.log('ggggggggggggggggg',i,tab[index],index); */
        return tab[index];
    }

    LoggedInPro() {
        let token: any = localStorage.getItem('token_Pro');
        if (!token) {
            return false;
        }

        if (this.helper.isTokenExpired(token)) {
            return false;
        }

        return true;
    }
}
