import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListInvitationsRoutingModule } from './list-invitations-routing.module';
import { ListInvitationsComponent } from './list-invitations/list-invitations.component';


@NgModule({
  declarations: [
    ListInvitationsComponent
  ],
  imports: [
    CommonModule,
    ListInvitationsRoutingModule
  ]
})
export class ListInvitationsModule { }
