import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListInvitationsComponent } from './list-invitations/list-invitations.component';

const routes: Routes = [
    {path:'',component:ListInvitationsComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListInvitationsRoutingModule { }
