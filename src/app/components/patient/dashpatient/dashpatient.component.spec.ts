import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DashpatientComponent } from './dashpatient.component';

describe('DashpatientComponent', () => {
  let component: DashpatientComponent;
  let fixture: ComponentFixture<DashpatientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DashpatientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DashpatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
