import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DashpatientComponent } from './dashpatient.component';

const routes: Routes = [{path:'',component:DashpatientComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashpatientRoutingModule { }
