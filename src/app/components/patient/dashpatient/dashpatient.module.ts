import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashpatientRoutingModule } from './dashpatient-routing.module';
import { DashpatientComponent } from './dashpatient.component';


@NgModule({
  declarations: [
    DashpatientComponent
  ],
  imports: [
    CommonModule,
    DashpatientRoutingModule
  ]
})
export class DashpatientModule { }
