import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListProfComponent } from './list-prof/list-prof.component';

const routes: Routes = [{path:'',component:ListProfComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ListProfRoutingModule { }
