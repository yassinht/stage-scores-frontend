import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListProfRoutingModule } from './list-prof-routing.module';
import { ListProfComponent } from './list-prof/list-prof.component';


@NgModule({
  declarations: [
    ListProfComponent
  ],
  imports: [
    CommonModule,
    ListProfRoutingModule
  ]
})
export class ListProfModule { }
