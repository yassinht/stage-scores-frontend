import { Component, OnInit } from '@angular/core';
import { LoginModel } from 'src/app/models/login';
import { AuthprofService } from 'src/app/service/authprof.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  role: string = '';
  public login: LoginModel;
  public userList = [];
  public patient: any;


  constructor(public _authprof: AuthprofService) {

  }
  ngOnInit(): void {
  }

  updateRole(selectedRole: string): void {
    this.role = selectedRole;
  }
  register(){
    this._authprof.createNewPatient(this.patient)
    .subscribe(
        res=>{
            console.log(res);
        },
        err=>{
            console.log(err);
        }
    );
  }


}
