import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { AuthentfRoutingModule } from './authentf-routing.module';
import { AuthentfComponent } from './authentf/authentf.component';


@NgModule({
  declarations: [
    AuthentfComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    AuthentfRoutingModule
  ]
})
export class AuthentfModule { }

