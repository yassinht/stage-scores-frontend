import { Component, OnInit } from '@angular/core';
import { LoginModel } from 'src/app/models/login'; // Check the path to LoginModel
import { NgForm } from '@angular/forms';
import { UserService } from 'src/app/user.service'; // Check the path to UserService

@Component({
  selector: 'app-authentf',
  templateUrl: './authentf.component.html',
  styleUrls: ['./authentf.component.scss']
})
export class AuthentfComponent implements OnInit {
  public login: LoginModel = new LoginModel(); // Initialize login here
  public userList = [];
  public professionel: any; // Corrected spelling from 'professionnel'

  constructor(private userService: UserService) { // Corrected parameter name
  }

  ngOnInit(): void {
    this.getUser();
  }

  authentf(f: NgForm) {
    let data = f.value;
    this.userService.login(data).subscribe(
      response => console.log(response),
      error => console.log(error)
    );
  }

  onSubmit(form: NgForm) {
    console.log(this.login);
  }

  getUser() {
    this.userService.getUserFromPlaceholder().subscribe(
      result => {
        this.userList = result;
        console.log(result);
      }
    );
  }

  ajout() {
    this.userService.createNewprof(this.professionel) // Corrected variable name
      .subscribe(
        res => {
          console.log(res);
        },
        err => {
          console.log(err);
        }
      );
  }
}
