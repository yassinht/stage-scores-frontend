import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthentfComponent } from './authentf.component';

describe('AuthentfComponent', () => {
  let component: AuthentfComponent;
  let fixture: ComponentFixture<AuthentfComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AuthentfComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthentfComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
