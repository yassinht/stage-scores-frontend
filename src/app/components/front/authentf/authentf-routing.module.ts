import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthentfComponent } from './authentf/authentf.component';


const routes: Routes = [{path:'',component:AuthentfComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AuthentfRoutingModule { }
