import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PatientComponent } from './patient.component';

const routes: Routes = [ {path:'',component:PatientComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PatientRoutingModule { }
export interface Patient {
    nom: string;
    prenom: string;
    email: string;
    num_identite: string;
    matricule_employé: string;
    adresse: string;
    Telephone: string;
    dateNaissance: string;
    poids_kg: number;
    taille_cm: number;
    num_sécurité_social: string;
  }
  