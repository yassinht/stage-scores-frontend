import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.scss']
})
export class PatientComponent implements OnInit {
  patients: any[] = [];

  constructor(private http: HttpClient) { }

  ngOnInit(): void {
    this.fetchPatients();
  }

  fetchPatients() {
    this.http.get<any[]>('http://localhost:5000/patient').subscribe(
      (data) => {
        this.patients = data;
      },
      (error) => {
        console.error('Error fetching patients:', error);
      }
    );
  }
}
