import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsProComponent } from './forms-pro.component';

describe('FormsProComponent', () => {
  let component: FormsProComponent;
  let fixture: ComponentFixture<FormsProComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormsProComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormsProComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
