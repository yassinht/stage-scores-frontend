import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FormsProRoutingModule } from './forms-pro-routing.module';
import { FormsProComponent } from './forms-pro/forms-pro.component';


@NgModule({
  declarations: [
    FormsProComponent
  ],
  imports: [
    CommonModule,
    FormsProRoutingModule
  ]
})
export class FormsProModule { }
