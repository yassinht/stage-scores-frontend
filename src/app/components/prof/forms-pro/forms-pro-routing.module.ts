import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsProComponent } from './forms-pro/forms-pro.component';

const routes: Routes = [{path:'',component:FormsProComponent}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsProRoutingModule { }
