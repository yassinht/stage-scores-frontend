import { RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { FormLayoutComponent } from './components/formlayout/formlayout.component';
import { PanelsComponent } from './components/panels/panels.component';
import { OverlaysComponent } from './components/overlays/overlays.component';
import { MediaComponent } from './components/media/media.component';
import { MessagesComponent } from './components/messages/messages.component';
import { MiscComponent } from './components/misc/misc.component';
import { EmptyComponent } from './components/empty/empty.component';
import { ChartsComponent } from './components/charts/charts.component';
import { FileComponent } from './components/file/file.component';
import { DocumentationComponent } from './components/documentation/documentation.component';
import { AppMainComponent } from './app.main.component';
import { InputComponent } from './components/input/input.component';
import { ButtonComponent } from './components/button/button.component';
import { TableComponent } from './components/table/table.component';
import { ListComponent } from './components/list/list.component';
import { TreeComponent } from './components/tree/tree.component';
import { CrudComponent } from './components/crud/crud.component';
import { BlocksComponent } from './components/blocks/blocks.component';
import { FloatLabelComponent } from './components/floatlabel/floatlabel.component';
import { InvalidStateComponent } from './components/invalidstate/invalidstate.component';
import { TimelineComponent } from './components/timeline/timeline.component';
import { IconsComponent } from './components/icons/icons.component';
import { ProfLayoutComponent } from './layouts/prof-layout/prof-layout.component';
import { FrontLayoutComponent } from './layouts/front-layout/front-layout.component';
import { PatientLayoutComponent } from './layouts/patient-layout/patient-layout.component';

@NgModule({
    imports: [
        RouterModule.forRoot([
            {
                path: 'dashboard', component: AppMainComponent,

            },
            {
                path: '', component: FrontLayoutComponent, children: [
                    { path: 'home', loadChildren: () => import('./components/front/home/home.module').then(m => m.HomeModule) },
                    { path: 'authentf', loadChildren: () => import('./components/front/authentf/authentf.module').then(m => m.AuthentfModule) },
                    { path: 'register', loadChildren: () => import('./components/front/register/register.module').then(m => m.RegisterModule) },
                    { path: 'about', loadChildren: () => import('./components/front/about-us/about-us.module').then(m => m.AboutUsModule) },
                    { path: 'contact', loadChildren: () => import('./components/front/contact/contact.module').then(m => m.ContactModule) },
                    { path: 'patient', loadChildren: () => import('./components/front/patient/patient.module').then(m => m.PatientModule) },
                ]
            },
            {
                path: 'professionnel', component: ProfLayoutComponent, children: [
                    { path: 'profil', loadChildren: () => import('./components/prof/profil-pro/profil-pro.module').then(m => m.ProfilProModule) },
                    { path: 'forms', loadChildren: () => import('./components/prof/forms-pro/forms-pro.module').then(m => m.FormsProModule) },
                    { path: 'patients-list', loadChildren: () => import('./components/prof/patients-list/patients-list.module').then(m => m.PatientsListModule) },
                ]
            },





            {path: '**', redirectTo: 'pages/empty'},
        ], {scrollPositionRestoration: 'enabled'})
    ],
    exports: [RouterModule]
})
export class AppRoutingModule {
}
